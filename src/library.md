# Game Library

A game library contains all games. A library can have multiple root directories.
Each library root contains a game directory for each installed game.
Each game directory must contain a file named `game.json`, called the [configuration file](game_config.md).
