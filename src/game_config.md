# Game Configuration

## Configuration file structure

A `game.json` contains a JSON object.

This object must have the following fields:

| Key           | Type        | Description             |
| ------------- | ----------- | ----------------------- |
| name          | string      | The name of the game    |

It may also have some of the following fields:

| Key           | Type        | Description                                                                                             |
| ------------- | ----------- | ------------------------------------------------------------------------------------------------------- |
| year          | integer     | The year the game was released                                                                          |
| wiki_url      | string      | URL of the Wikipedia page of the game                                                                   |
| env_vars      | object      | An object with key-value pairs for each environment variable that will be set before launching the game |

It must also have a runner field. Available runners are: [wine](runner/wine.md), [native](runner/native.md).

