# Examples

- fallout-2/
```
{
	"name": "Fallout 2",
	"year": 2007,
	"wine": {
		"version": "system",
		"executable_path": "game/files/fallout2HR.exe",
		"dll-overrides": {
			"d3d9": "n,b"
		},
		"winetricks": [
			"dxvk"
		]
	},
	"env_vars": {
		"WINEESYNC": 1,
		"WINEFSYNC": 1,
		"WINEDEBUG": "-all",
		"WINE_LARGE_ADDRESS_AWARE": "1",
		"WINEPREFIX": "./game/prefix"
	}
}
```

- fallout/
```
{
	"name": "Fallout",
	"year": 2007,
	"wine": {
		"version": "system",
		"executable_path": "game/files/falloutwHR.exe",
		"dll-overrides": {
			"d3d9": "n,b"
		},
		"winetricks": [
			"dxvk"
		]
	},
	"env_vars": {
		"WINEESYNC": 1,
		"WINEFSYNC": 1,
		"WINEDEBUG": "-all",
		"WINE_LARGE_ADDRESS_AWARE": "1",
		"WINEPREFIX": "./game/prefix"
	}
}
```

- far-cry-2-fortunes-edition/
```
{
	"name": "Far Cry 2",
	"year": 2007,
	"wiki_url": "https://en.wikipedia.org/wiki/Far_Cry_2",
	"wine": {
		"version": "system",
		"executable_path": "game/files/FarCry2MFLauncher.exe",
		"dll-overrides": {
			"d3d9": "n,b"
		},
		"winetricks": [
			"dxvk"
		]
	},
	"env_vars": {
		"WINEESYNC": 1,
		"WINEFSYNC": 1,
		"WINEDEBUG": "-all",
		"WINE_LARGE_ADDRESS_AWARE": "1",
		"WINEPREFIX": "./game/prefix"
	}
}
```

- far-cry/
```
{
	"name": "Far Cry",
	"year": 2007,
	"wine": {
		"version": "system",
		"executable_path": "game/files/FarCry.exe",
		"dll-overrides": {
			"d3d9": "n,b"
		},
		"winetricks": [
			"dxvk"
		]
	},
	"env_vars": {
		"WINEESYNC": 1,
		"WINEFSYNC": 1,
		"WINEDEBUG": "-all",
		"WINE_LARGE_ADDRESS_AWARE": "1",
		"WINEPREFIX": "./game/prefix"
	}
}
```

- grand-theft-auto-san-andreas/
```
{
	"name": "Grand Theft Auto San Andreas",
	"year": 2007,
	"wiki_url": "https://en.wikipedia.org/wiki/Grand_Theft_Auto:_San_Andreas",
	"wine": {
		"version": "system",
		"executable_path": "./game/files/gta-sa.exe",
		"dll-overrides": {
			"d3d9": "n,b"
		},
		"winetricks": [
			"dxvk"
		]
	},
	"env_vars": {
		"WINEESYNC": "1",
		"WINEFSYNC": "1",
		"WINEDEBUG": "-all",
		"WINE_LARGE_ADDRESS_AWARE": "1",
		"WINEPREFIX": "./game/prefix"
	}
}
```

- grand-theft-auto-vice-city/
```
{
	"name": "Grand Theft Auto Vice City",
	"year": 2007,
	"wiki_url": "https://en.wikipedia.org/wiki/Grand_Theft_Auto:_Vice_City",
	"wine": {
		"version": "system",
		"executable_path": "./game/files/gta-vc.exe",
		"args": [
			"-norestrictions",
			"-nomemrestrict",
			"-percentvidmem 100",
			"-novblank"
		],
		"dll-overrides": {
			"d3d9": "n,b"
		},
		"winetricks": [
			"dxvk"
		]
	},
	"env_vars": {
		"WINEESYNC": 1,
		"WINEFSYNC": 1,
		"WINEDEBUG": "-all",
		"WINE_LARGE_ADDRESS_AWARE": "1",
		"WINEPREFIX": "./game/prefix"
	}
}
```

- minecraft/
```
{
	"name": "Minecraft",
	"year": 2007,
	"wiki_url": "https://en.wikipedia.org/wiki/Minecraft",
	"native": {
		"executable_path": "start.sh"
	}
}
```
