# Summary

- [Game Library](./library.md)
- [Game Configuration](./game_config.md)
    - [Examples](./config_examples.md)
    - [Runners](./runners.md)
        - [Wine](./runner/wine.md)
        - [Native](./runner/native.md)
