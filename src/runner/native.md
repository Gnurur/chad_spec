# Native Runner

Runs a native game.

## Configuration

This object must have the following fields:

| Key               | Type        | Description                                         |
| -------------     | ----------- | ------------------------                            |
| executable_path   | string      | Path to the game executable                         |

It may also have some of the following fields:

| Key               | Type        | Description                                         |
| -------------     | ----------- | ------------------------                            |
