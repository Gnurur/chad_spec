# Wine Runner

Runs the game using Wine.

## Configuration

This object must have the following fields:

| Key               | Type        | Description                                         |
| -------------     | ----------- | ------------------------                            |
| version           | string      | [Wine version](#version)                            |
| executable_path   | string      | Path to the game executable                         |

It may also have some of the following fields:

| Key               | Type        | Description                                         |
| -------------     | ----------- | ------------------------                            |
| dll-overrides     | object      | Object with key-value pairs for each dll override   |
| winetricks        | array       | List of all winetricks verbs that must be applied   |

### Version

Version of wine to use. Possible values: `system`
